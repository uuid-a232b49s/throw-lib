cmake_minimum_required(VERSION 3.14)

set(_pretty_funcs
    __PRETTY_FUNCTION__
    __FUNCTION__
    __func__
)

unset(_pretty_func)
foreach(fn IN LISTS _pretty_funcs)
    set(_r _throwlib_find_pretty_func_${fn})
    try_compile(${_r}
        "${CMAKE_CURRENT_BINARY_DIR}/pretty-function"
        SOURCES "${CMAKE_CURRENT_LIST_DIR}/find-pretty-func.cxx"
        COMPILE_DEFINITIONS -DCURRENT_FUNCTION=${fn}
    )
    if(${${_r}})
        set(_pretty_func ${fn})
        break()
    endif()
endforeach()

if(NOT DEFINED _pretty_func)
    set(THROWLIB_pretty_func_ "\"unknown\"" CACHE INTERNAL "" FORCE)
else()
    set(THROWLIB_pretty_func_ "${_pretty_func}" CACHE INTERNAL "" FORCE)
endif()
