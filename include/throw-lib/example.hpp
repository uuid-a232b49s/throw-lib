#ifndef HEADER_THROWLIB_INTERNAL_EXAMPLE
#define HEADER_THROWLIB_INTERNAL_EXAMPLE 1

#include <exception>

namespace throw_lib {

    class LibException : public std::exception {
    public:
        virtual char const * what() const noexcept override = 0;
        virtual const char * file() const noexcept = 0;
        virtual signed long long line() const noexcept = 0;
        virtual const char * func() const noexcept = 0;
        virtual int print(char * buffer, unsigned size) const noexcept = 0;
    };

    class LibExceptionEx : public std::exception {
    public:
        void * m_extra;
        LibExceptionEx(void * ex) : m_extra(ex) {}
        virtual char const * what() const noexcept override = 0;
        virtual const char * file() const noexcept = 0;
        virtual signed long long line() const noexcept = 0;
        virtual const char * func() const noexcept = 0;
        virtual int print(char * buffer, unsigned size) const noexcept = 0;
    };

}

#endif
