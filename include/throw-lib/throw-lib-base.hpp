#ifndef HEADER_THROWLIB_BASE
#define HEADER_THROWLIB_BASE

#include <cstdio>
#include <string>
#include <utility>

namespace throw_lib {
    namespace details {

        void print_exc(const char * msg, const char * file, signed long long line, const char * func) noexcept;

        template<typename string_t>
        struct string_storage_t;

        template<typename elem_t, typename traits_t, typename alloc_t>
        struct string_storage_t<std::basic_string<elem_t, traits_t, alloc_t>> : public std::basic_string<elem_t, traits_t, alloc_t> {
            using std::basic_string<elem_t, traits_t, alloc_t>::basic_string;
            string_storage_t() = default;
            string_storage_t(std::basic_string<elem_t, traits_t, alloc_t> s)
                : std::basic_string<elem_t, traits_t, alloc_t>(std::move(s)) {}
        };

        template<>
        struct string_storage_t<const char *> {
            const char * m_value;
            string_storage_t(const char * v) noexcept : m_value(v) {}
            const char * c_str() const noexcept { return m_value; }
        };

        template<>
        struct string_storage_t<char *> : string_storage_t<const char *> {
            using string_storage_t<const char *>::string_storage_t;
        };

        template<typename exc_t, typename msg_t_, typename file_trim>
        class exception_impl0 : public exc_t {
        public:
            using msg_t = msg_t_;

            string_storage_t<msg_t> m_msg;
            const char * m_file;
            signed long long m_line;
            const char * m_func;

            template<typename ... args_t>
            exception_impl0(
                msg_t msg,
                const char * file,
                signed long long line,
                const char * func,
                args_t && ... args
            ) : exc_t(std::forward<args_t>(args)...),
                m_msg(std::move(msg)),
                m_file(file_trim::trim(file)),
                m_line(line),
                m_func(func) {}

            virtual char const * what() const noexcept override { return m_msg.c_str(); }
            virtual const char * file() const noexcept override { return m_file; }
            virtual signed long long line() const noexcept override { return m_line; }
            virtual const char * func() const noexcept override { return m_func; }

            virtual int print(char * buffer, unsigned size) const noexcept override {
                return std::snprintf(
                    buffer, size,
                    "%s at <%s> [%s:%lli]",
                    m_msg.c_str(), m_func, m_file, m_line
                );
            }

        };

    }
}

#define ASSERT(cnd, exc, msg) ( (!!(cnd)) ? ((void)0) : (THROW(exc, (msg) )) )
#define ASSERT_EX(cnd, exc, msg, ...) ( (!!(cnd)) ? ((void)0) : (THROW_EX(exc, (msg), __VA_ARGS__ )) )

#endif
