#include <iostream>

namespace throw_lib {
    namespace details {

        void print_exc(const char * msg, const char * file, signed long long line, const char * func) noexcept {
            try {
                std::cerr << msg << " at <" << func << "> [" << file << ":" << line << "]" << std::endl;
            } catch (...) {}
        }

    }
}
