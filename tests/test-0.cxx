#include <cassert>
#include <cstring>

#include "throw-lib/example.hpp"
#include "throw-lib/throw-test-0/throw-lib.hpp"

int main(int argc, char ** argv) {
    try {
        THROW(throw_lib::LibException, "msg ^^");
    } catch (throw_lib::LibException & ex) {
        assert(std::strcmp(ex.what(), "msg ^^") == 0);
        assert(ex.line() == 9);
    }
    try {
        THROW_EX(throw_lib::LibExceptionEx, "msg ^^", nullptr);
    } catch (throw_lib::LibException & ex) {
        assert(std::strcmp(ex.what(), "msg ^^") == 0);
        assert(ex.line() == 9);
    }
    return 0;
}
